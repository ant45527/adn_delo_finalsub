<?php

/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/10/2017
 * Time: 3:44 PM
 */
class Activity_Three_View
{

    // Create the view
    public function __construct()
    {
        return $this->__toString();
    }

    // Build the HTML
    public function __toString()
    {
        // Create the login/registration container
        $container = "<div id='activity_three_view'>
                <h2>Create an Account to Store/Retrieve a Secret String</h2>
                <div id='register_container'>
                    <p>Register</p></br>
                    <label>Email:</label>
                    <input id='register_email' type='text'></br>
                    <label>Password:</label>
                    <input id='register_password' type='password'></br>
                    <label>Confirm Password:</label>
                    <input id='confirm_password' type='password'></br>
                    <p id='invalid_registration_message'></p>
                    <button id='register_button' type='button' onclick='registerUser();'>Register</button>
                </div>
                <div id='login_container'>
                    <p>Login</p></br>
                    <label>Email:</label>
                    <input id='login_email' type='text'></br>
                    <label>Password:</label>
                    <input id='login_password' type='password'></br>
                    <p id='invalid_login_message'></p>
                    <button id='login_button' type='button' onclick='login();'>Login</button>
                </div>
                </div>";

        // Return the form
        return $container;
    }

    // Get the secret string view/editing container
    public static function getSecretStringPopup() {
        // Create the clickout
        $clickout = "<div id='secret_string_clickout' class='clickout' onclick='closeSecretPopup();'></div>";

        // Try to get a secret to display
        $secret = self::getSecretStringForCurrentUser();

        // Create the container
        $container = "<div id='secret_string_view'>
                <h2>Your Secret String</h2><br>
                <hr><br>
                <label>Secret String:</label><br>
                <input id='secret_string' type='text' value=$secret></br><br>
                <p id='invalid_secret_message'></p><br>
                <button id='update_secret_button' type='button' onclick='saveSecret(this);'>Save</button>
                </div>";

        // Return the clickout and container
        return $clickout . $container;
    }

    public static function getSecretStringForCurrentUser() {
        // Get the current user's id
        $userId = $_SESSION["id_user"];

        // Create the sqler
        $sqler = new SQLer();

        // Send the query
        $sqler->sendQuery("Select secret from secret where secret.userId=$userId");

        // If found a secret
        if($row = $sqler->getRow()) {
            return $row["secret"];
        }
        // Otherwise return an empty string
        else {
            return "";
        }
    }
}