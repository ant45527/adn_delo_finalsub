<?php

/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/7/2017
 * Time: 10:07 AM
 */
class Activity_One_View
{
    // Construct the view
    public function __construct() {
        return $this->__toString();
    }

    // Get the html file to utilize and return its contents
    public function __toString()
    {
        return file_get_contents('../html/flags.html', FILE_USE_INCLUDE_PATH);
    }
}