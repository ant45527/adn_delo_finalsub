<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/7/2017
 * Time: 9:05 AM
 */

spl_autoload_register(function ($class) {
    include 'php/' . $class . '.class.php';
});

// Create the head
$head = "<head>
            <link rel='stylesheet' type='text/css' href='css/main.css'>
            <script type='text/javascript' src='js/jquery-latest.js'></script>
            <script type='text/javascript' src='js/main.js'></script>
            <title>ADN DELO Final Activities Submission</title>
        </head>";

// Create the body
$body = "<body>" .
        new Activity_Menu()
        . "</body>";

// Create the footer
$footer = "<footer>Developed by Drew Norman - January 2017</footer>";

// Echo the HTML
echo $head . $body . $footer;