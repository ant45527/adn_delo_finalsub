/**
 * Created by Drew on 1/7/2017.
 */

// On page load
$(document).ready(function() {
    // Fade in the body
    $("body").fadeIn(500);
});

// Submits the user's secret to the database
function saveSecret() {
    var secret = $("#secret_string").val();

    clearInvalidInputNotifiers();

    // Check if too long
    if (secret.length > 255) {
        $("#secret_string").css({ 'border-color': 'red'});
        $("#invalid_secret_message").text("The given secret is too long (255 chars max)!");
        return;
    }

    $.ajax({
        url: 'ajax/save_secret.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            secret: (secret)
        },
        success: function(data) {
            var response = JSON.parse(data);
            if (response["success"]) {
                alert("Secret saved successfully!");
            }
            else {
                $("#secret_string").css({ 'border-color': 'red'});
                $("#invalid_secret_message").text(response["reason"]);
            }
        },
        error: function() {
            console.log("Error with saving secret!");
        }
    });
}

// Submits the user registration for activity three
function registerUser() {
    var email = $("#register_email").val();
    var password = $("#register_password").val();
    var confirmPass = $("#confirm_password").val();

    clearInvalidInputNotifiers();

    if (email.length === 0) {
        $("#register_email").css({ 'border-color': 'red'});
        $("#invalid_registration_message").text("The email field cannot be empty!");
        return;
    }

    if (!email.includes("@wku.edu") && !email.includes("@topper.wku.edu")) {
        $("#register_email").css({ 'border-color': 'red'});
        $("#invalid_registration_message").text("The provided email must be a TopperMail account!");
        return;
    }

    if (password.length === 0) {
        $("#register_password").css({ 'border-color': 'red'});
        $("#invalid_registration_message").text("The password field cannot be empty!");
        return;
    }

    if (password.length < 8) {
        $("#register_password").css({ 'border-color': 'red'});
        $("#invalid_registration_message").text("The given password must be at least 8 characters long!");
        return;
    }

    if (password.length >= 25) {
        $("#register_password").css({ 'border-color': 'red'});
        $("#invalid_registration_message").text("The given password must be less than 25 characters long!");
        return;
    }

    if (password !== confirmPass) {
        $("#register_password").css({ 'border-color': 'red'});
        $("#confirm_password").css({ 'border-color': 'red'});
        $("#invalid_registration_message").text("The given passwords do not match!");
        return;
    }

    $.ajax({
        url: 'ajax/register.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            email: (email),
            password: (password),
            confirmPass: (confirmPass)
        },
        success: function(data) {
            var response = JSON.parse(data);
            if (response["success"]) {
                alert("Registration successful! You may now login.");
                $("#register_email").val(""); // Clear the register email input
                $("#register_password").val(""); // Clear the register password input
                $("#confirm_password").val(""); // Clear the confirm password input
                $("#login_email").val(email); // Set the login email input to the registered input
            }
            else {
                $("#register_email").css({ 'border-color': 'red'});
                $("#register_password").css({ 'border-color': 'red'});
                $("#confirm_password").css({ 'border-color': 'red'});
                $("#invalid_registration_message").text(response["reason"]);
            }
        },
        error: function() {
            console.log("Error with registering new user!");
        }
    });
}

// Submits the user login for activity three
function login() {
    var email = $("#login_email").val();
    var password = $("#login_password").val();

    clearInvalidInputNotifiers();

    if (email.length === 0) {
        $("#login_email").css({ 'border-color': 'red'});
        $("#invalid_login_message").text("Please enter your email!");
        return;
    }

    if (password.length === 0) {
        $("#login_password").css({ 'border-color': 'red'});
        $("#invalid_login_message").text("Please enter your password!");
        return;
    }

    $.ajax({
        url: 'ajax/login.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            email: (email),
            password: (password)
        },
        success: function(data) {
            var response = JSON.parse(data);
            if (response["success"]) {
                console.log("Login successful!");

                // Append the secret clickout and view
                $("body").append(response["toAppend"]);

                // Fade in the secret clickout
                $("#secret_string_clickout").fadeIn(500);

                // Fade in the secret view
                $("#secret_string_view").fadeIn(500);

            }
            else {
                $("#login_email").css({ 'border-color': 'red'});
                $("#login_password").css({ 'border-color': 'red'});
                $("#invalid_login_message").text(response["reason"]);
            }
        },
        error: function() {
            console.log("Error with login!");
        }
    });
}

// Closes the secret view
function closeSecretPopup() {
    // Fade out then remove the secret string clickout
    $("#secret_string_clickout").fadeOut(500, function() {
        $(this).remove();
    });

    // Fade out then remove the secret string view
    $("#secret_string_view").fadeOut(500, function() {
        $(this).remove();
    });
}

// Closes the successful course registration view
function closeSuccessfulRegistrationPopup() {
    // Fade out then remove the successful registration clickout
    $("#successful_course_registration_clickout").fadeOut(500, function() {
        $(this).remove();
    });

    // Fade out then remove the successful registration view
    $("#successful_course_registration_view").fadeOut(500, function() {
        $(this).remove();
    });
}

// Clear all invalid input messages and color changes
function clearInvalidInputNotifiers() {
    $("#invalid_registration_message").text("");
    $("#invalid_login_message").text("");
    $("#invalid_form_message").text("");
    $("#invalid_secret_message").text("");
    $("input").css("border-color", "grey");
}

// Submits the course registration form for activity two
function registerForCourse() {
    // Get the inputs
    var firstName = $("#first_name_input").val();
    var lastName = $("#last_name_input").val();
    var wkuId = $("#wku_id_input").val();
    var courseNumber = $("#course_number_input").val();
    var courseSemester = $("#course_semester_input").val();

    clearInvalidInputNotifiers();

    // Validate the form
    if (firstName.includes(" ")) {
        $("#first_name_input").css("border-color", "red");
        $("#invalid_form_message").text("Please no spaces in the first name!");
        return;
    }
    if (/\d/.test(firstName)) {
        $("#first_name_input").css("border-color", "red");
        $("#invalid_form_message").text("Please no numbers in the first name!");
        return;
    }
    if (firstName.length == 0) {
        $("#first_name_input").css("border-color", "red");
        $("#invalid_form_message").text("The first name field cannot be empty!");
        return;
    }
    if (lastName.includes(" ")) {
        $("#last_name_input").css("border-color", "red");
        $("#invalid_form_message").text("Please no spaces in the last name!");
        return;
    }
    if (/\d/.test(lastName)) {
        $("#last_name_input").css("border-color", "red");
        $("#invalid_form_message").text("Please no numbers in the last name!");
        return;
    }
    if (lastName.length == 0) {
        $("#last_name_input").css("border-color", "red");
        $("#invalid_form_message").text("The last name field cannot be empty!");
        return;
    }
    if (wkuId.substring(0, 3) != "800" || wkuId.length != 9) {
        $("#wku_id_input").css("border-color", "red");
        $("#invalid_form_message").text("The provided WKU ID is not valid.");
        return;
    }
    if (courseNumber.length != 6 || !parseInt(courseNumber)) {
        $("#course_number_input").css("border-color", "red");
        $("#invalid_form_message").text("The provided course number is not a six-digit number.");
        return;
    }

    // Submit for server-side validation
    $.ajax({
        url: 'ajax/register_for_course.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            firstName: (firstName),
            lastName: (lastName),
            wkuId: (wkuId),
            courseNumber: (courseNumber),
            courseSemester: (courseSemester)
        },
        success: function(data) {
            var response = JSON.parse(data);
            if (response["success"]) {
                // Append the successful registration container and clickout
                $("body").append(response["toAppend"]);

                // Fade in the successful registration clickout
                $("#successful_course_registration_clickout").fadeIn(500);

                // Fade in the successful registration container
                $("#successful_course_registration_view").fadeIn(500);
            }
            else {
                alert(response["reason"]);
            }
        },
        error: function() {
            console.log("Error with registering for course!");
        }
    });

}

// Displays the appropriate activity
function viewActivity(button) {
    // Determine the activity id
    var id = $(button).data("activity-id");

    console.log("Displaying activity " + id + "...");

    // Retrieve the content to display
    $.ajax({
        url: 'ajax/view_activity.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            activityId: (id)
        },
        success: function(data) {
            // Fade the body out
            $("body").fadeOut(500, function () {
                // Then replace the content and fade it back in
                $("body").empty().append(data).fadeIn(500);
            });
        },
        error: function(data) {
            console.log("Error with displaying activity" + id + ": ", data);
        }
    });
}

// Returns to the activity menu from any activity view
function returnToMenu() {
    // Fade the body out
    $("body").fadeOut(500, function () {
        // Reload the page
        window.location.reload();
    })
}