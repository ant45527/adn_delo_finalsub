<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/10/2017
 * Time: 4:17 PM
 */

include "../php/sqler.class.php";
include_once "../php/activity_three_view.class.php";

session_start();

$sqler = new sqler();

// Create the response array to json_encode and echo
$response = [];

// Filter the inputs from post
$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);

// Validate the input quality
if (!isset($email) || strlen($email) == 0 || (strpos($email,"@wku.edu") === FALSE && strpos($email,"@topper.wku.edu") === FALSE)) {
    $response = ["success" => FALSE, "reason" => "The given email is not a TopperMail account."];
    echo json_encode($response);
    return;
}
if (!isset($password) || strlen($password) == 0 || strlen($password) < 8) {
    $response = ["success" => FALSE, "reason" => "The given password doesn't appear valid."];
    echo json_encode($response);
    return;
}


$hashedPass = $sqler->hashPass($password);

$sqler->sendQuery("Select id from user where user.email='$email' and user.password='$hashedPass'");

if($row = $sqler->getRow())
{
    // Update the SESSION to reflect the new user
    $_SESSION["id_user"] = $row["id"];

    // Echo the response including the secret string popup
    $response = ["success" => TRUE, "toAppend" => Activity_Three_View::getSecretStringPopup()];
    echo json_encode($response);
    return;
}
else
{
    // Echo the failure response
    $response = ["success" => FALSE, "reason" => "The provided credentials are invalid!"];
    echo json_encode($response);
    return;
}
