<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/10/2017
 * Time: 4:55 PM
 */

include_once "../php/activity_two_view.class.php";

// Create the response array to json_encode and echo
$response = [];

// Filter the inputs
$firstName = filter_input(INPUT_POST, "firstName", FILTER_SANITIZE_STRING);
$lastName = filter_input(INPUT_POST, "lastName", FILTER_SANITIZE_STRING);
$wkuId = filter_input(INPUT_POST, "wkuId", FILTER_SANITIZE_STRING);
$courseNumber = filter_input(INPUT_POST, "courseNumber", FILTER_SANITIZE_STRING);
$courseSemester = filter_input(INPUT_POST, "courseSemester", FILTER_SANITIZE_STRING);

// Validate the input quality
if (!isset($firstName) || strlen($firstName) == 0 || strpos($firstName," ") !== FALSE || preg_match('/\\d/', $firstName) > 0) {
    $response = ["success" => FALSE, "reason" => "The provided first name is invalid."];
    echo json_encode($response);
    return;
}
if (!isset($lastName) || strlen($lastName) == 0 || strpos($lastName," ") !== FALSE || preg_match('/\\d/', $lastName) > 0) {
    $response = ["success" => FALSE, "reason" => "The provided last name is invalid."];
    echo json_encode($response);
    return;
}
if (!isset($wkuId) || strlen($wkuId) != 9 || strpos($wkuId,"800") !== 0 || !is_numeric($wkuId)) {
    $response = ["success" => FALSE, "reason" => "The provided WKU ID is invalid."];
    echo json_encode($response);
    return;
}
if (!isset($courseNumber) || strlen($courseNumber) != 6 || !is_numeric($wkuId)) {
    $response = ["success" => FALSE, "reason" => "The provided course number is invalid."];
    echo json_encode($response);
    return;
}
if (!isset($courseSemester) || strlen($courseSemester) == 0) {
    $response = ["success" => FALSE, "reason" => "The provided course semester is invalid."];
    echo json_encode($response);
    return;
}

$toAppend = Activity_Two_View::getSuccessfulRegistrationPopup($firstName, $lastName, $wkuId, $courseNumber, $courseSemester);
$response = ["success" => TRUE, "toAppend" => $toAppend];
echo json_encode($response);