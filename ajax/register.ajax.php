<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/10/2017
 * Time: 4:13 PM
 */

include "../php/sqler.class.php";

$sqler = new sqler();

// Create the response array to json_encode and echo
$response = [];

$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$confirmPass = filter_input(INPUT_POST, "confirmPass", FILTER_SANITIZE_STRING);

// Validate the input quality
if (!isset($email) || strlen($email) == 0 || (strpos($email,"@wku.edu") === FALSE && strpos($email,"@topper.wku.edu") === FALSE )) {
    $response = ["success" => FALSE, "reason" => "The provided email doesn't appear to be a valid TopperMail account."];
    echo json_encode($response);
    return;
}
if (!isset($password) || strlen($password) == 0 || strlen($password) < 8) {
    $response = ["success" => FALSE, "reason" => "The provided password doesn't appear to match our registration standards."];
    echo json_encode($response);
    return;
}
if ($password !== $confirmPass) {
    $response = ["success" => FALSE, "reason" => "The provided passwords do not match."];
    echo json_encode($response);
    return;
}

$sqler->sendQuery("Select id from user where user.email='$email'");

// Duplicate email so refuse the registration
if ($sqler->getRow()) {
    $response = ["success" => FALSE, "reason" => "The provided email is already associated with an account."];
    echo json_encode($response);
    return;
}
else {
    if(!$stmt = $sqler->con->prepare("INSERT INTO user (email, password) VALUES (?,?)"))
    {
        $response = ["success" => FALSE, "reason" => "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error];
        echo json_encode($response);
        return;
    }


    if(!$stmt->bind_param("ss", $email, $password))
    {
        $response = ["success" => FALSE, "reason" => "Bind fail (" . $stmt->errno . ") " . $stmt->error];
        echo json_encode($response);
        return;
    }

    // Hash the provided password
    $password = $sqler->hashPass($password);

    if($stmt->execute())
    {
        $response = ["success" => TRUE];
        echo json_encode($response);
        return;
    }
    else
    {
        $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
        $stmt->close();
        $response = ["success" => FALSE, "reason" => $error];
        echo json_encode($response);
        return;
    }
}
