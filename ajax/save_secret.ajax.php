<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/10/2017
 * Time: 4:40 PM
 */

include "../php/sqler.class.php";

session_start();

$sqler = new sqler();

$secret = filter_input(INPUT_POST, "secret", FILTER_SANITIZE_STRING);
$userId =  $_SESSION["id_user"];

// Validate the input quality
if (!isset($secret)) {
    $secret = "";
}
if (strlen($secret) > 255) {
    $response = ["success" => FALSE, "reason" => "The given secret is too long (255 chars max)!"];
    echo json_encode($response);
    return;
}

$sqler->sendQuery("Select id from secret where secret.userId='$userId'");

// Duplicate user so update rather than create a new record
if ($row = $sqler->getRow()) {
    // Get the id to update
    $id = $row["id"];

    if(!$stmt = $sqler->con->prepare("UPDATE secret SET userId=?, secret=? WHERE id=?"))
    {
        $response = ["success" => FALSE, "reason" => "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error];
        echo json_encode($response);
        return;
    }

    if(!$stmt->bind_param("isi", $userId, $secret, $id))
    {
        $response = ["success" => FALSE, "reason" => "Bind fail (" . $stmt->errno . ") " . $stmt->error];
        echo json_encode($response);
        return;
    }

    if($stmt->execute())
    {
        $response = ["success" => TRUE];
        echo json_encode($response);
        return;
    }
    else
    {
        $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
        $stmt->close();
        $response = ["success" => FALSE, "reason" => $error];
        echo json_encode($response);
        return;
    }
}
else {
    if(!$stmt = $sqler->con->prepare("INSERT INTO secret (userId, secret) VALUES (?,?)"))
    {
        $response = ["success" => FALSE, "reason" => "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error];
        echo json_encode($response);
        return;
    }


    if(!$stmt->bind_param("is", $userId, $secret))
    {
        $response = ["success" => FALSE, "reason" => "Bind fail (" . $stmt->errno . ") " . $stmt->error];
        echo json_encode($response);
        return;
    }

    if($stmt->execute())
    {
        $response = ["success" => TRUE];
        echo json_encode($response);
        return;
    }
    else
    {
        $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
        $stmt->close();
        $response = ["success" => FALSE, "reason" => $error];
        echo json_encode($response);
        return;
    }
}
